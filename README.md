
# Flarum ext Mediaembed
在mediaembed中添加了b站、虾米、网易云、qq、爱奇艺等链接预览


### 安装
composer:

```sh
composer require monthwolf/flarum-mediaembed
```

### 更新
```sh
composer update monthwolf/flarum-mediaembed
```
  
  
  
#### 来源:https://gitee.com/echo886/flarum-mediaembed
#### 版权所有:[wuheihei](https://gitee.com/echo886)